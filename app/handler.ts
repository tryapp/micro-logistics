
import { Handler, Context } from 'aws-lambda';
import dotenv from 'dotenv';
import path from 'path';
const dotenvPath = path.join(__dirname, '../', `config/.env.${process.env.NODE_ENV}`);
dotenv.config({
  path: dotenvPath,
});

import { books, orders } from './model';
import { BooksController } from './controller/books';
import { OrdersController } from './controller/orders';

import { ClickEntregasOrdersController } from './controller/clickEntregasOrders';

import  OrderFactoryLalaMove from './service/factory/orderFactoryLalaMove';
//import { OrdersService } from './service/orders';
import DeliveriesScheduler from './service/schedulers/deliveriesScheduler';

import { MessageUtil } from './utils/message';

const booksController = new BooksController(books);
const ordersController = new OrdersController(orders);
const clickEntregasOrdersController = new ClickEntregasOrdersController();
const lalaMoveOrdersController = new OrderFactoryLalaMove();
//const ordersService = new OrdersService(orders);

// BOOKS
export const create: Handler = (event: any, context: Context) => {
  return booksController.create(event, context);
};

export const update: Handler = (event: any) => booksController.update(event);

export const find: Handler = () => booksController.find();

export const findOne: Handler = (event: any, context: Context) => {
  return booksController.findOne(event, context);
};

export const deleteOne: Handler = (event: any) => booksController.deleteOne(event);

// ORDERS

export const createOrder: Handler = (event: any, context: Context) => {
  console.log(event, ">>>>>>>>>>>>>>>>>");
  return ordersController.create(event, context);
};

export const updateOrder: Handler = (event: any, context: Context) => {
  return ordersController.update(event, context);
};

export const findOrders: Handler = (event: any) => ordersController.find(event);

export const findOneOrder: Handler = (event: any, context: Context) => {
  return ordersController.findOne(event, context);
};

export const findDriverOrder: Handler = (event: any, context: Context) => {
  return ordersController.findDriverOrder(event, context);
};

export { default as receiver } from './sqs/receiver';

// UDDATES ORDERS FROM CLICK ENTREGAS

export const updateClickEntregasOrder: Handler = (event: any, context: Context) => {
  return clickEntregasOrdersController.update(event, context);
};

export const updateLalaMoveOrder: Handler = (event: any, context: any) => {
  return lalaMoveOrdersController.update(event, context);
};
/*
export const updateLalaMoveOrder: Handler = (_id: string, context: Context) => {
  return ordersService.updateOrdersLalaMove(_id, context);
} 

*/
// cron delivery
export const cronDelivery: Handler = async (_: any, context: Context) => {
  const time = new Date();
  console.log(`---> Your cron function "${context.functionName}" ran at ${time}`);

  await new DeliveriesScheduler().process()

  return MessageUtil.success({message: 'success run cronDelivery'});
}
