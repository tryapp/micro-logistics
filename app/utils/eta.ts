import axios from 'axios';

export type ETAParams = {
  origin: string,
  destination: string,
}

export async function getETA(params: ETAParams){
  try {
    const url =`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${params.origin}&destinations=${params.destination}&key=${process.env.GOOGLE_API_KEY}`

    const response = await axios.get(url)

    const seconds = response.data.rows[0].elements[0].duration.value;

    return seconds / 60;
  } catch (error) {
    console.log('---> [ERROR] ETA', error)

    return 1000;
  }
}