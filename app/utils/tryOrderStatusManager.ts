export const getOrderNextStatus = (
  payloadStatus: string,
  currentStatus: string,
  deliveryWay: string = ""
): string | undefined => {
  //deliveryWay = deliveryWay.split('_')[0];
  switch (payloadStatus) {
    case "courier_assigned":
      return processCourierAssigned(currentStatus);
    case "courier_departed":
      return processCourierDeparted(currentStatus, deliveryWay);
    case "parcel_picked_up":
      return processParcelPickedUp(currentStatus, deliveryWay);
    case "active":
      return processActive(currentStatus, deliveryWay);
    case "courier_arrived":
      return undefined;
    case "finished":
      return processFinished(currentStatus, deliveryWay);
    default:
      return undefined;
  }
};
export const getOrderNextStatusFactory = (
  payloadStatus: string,
  currentStatus: string,
  deliveryWay: string = ""
): string | undefined => {
  return nextStatus(payloadStatus, currentStatus, deliveryWay);
  // - Status LALAMove, - Status do banco
};

function processCourierAssigned(currentStatus: string): string | undefined {
  return currentStatus === "planned" ? "ACCEPTED" : undefined;
}

function processCourierDeparted(
  currentStatus: string,
  deliveryWay: string
): string | undefined {
  return currentStatus === "courier_assigned"
    ? `REACHED_${deliveryWay}`
    : undefined;
}

function processParcelPickedUp(
  currentStatus: string,
  deliveryWay: string
): string | undefined {
  return currentStatus === "courier_departed"
    ? `GOT_BAG_${deliveryWay}`
    : undefined;
}

function processActive(
  currentStatus: string,
  deliveryWay: string
): string | undefined {
  return currentStatus === "parcel_picked_up"
    ? `BAG_PICKED_${deliveryWay}`
    : undefined;
}

function processFinished(
  currentStatus: string,
  deliveryWay: string
): string | undefined {
  return currentStatus === "courier_arrived"
    ? `BAG_DELIVERED_${deliveryWay === "STORE" ? "CUSTOMER" : "STORE"}`
    : undefined;
}

function nextStatus(
  typeUpdate: string,
  payloadStatus: string,
  deliveryWay?: string
): string | undefined {
  let status = "";
  console.log('statusaqui', typeUpdate)
  switch (typeUpdate.toLowerCase()) {
    case "on_going":
      return "ACCEPTED";
    case "picked_up":
      const deliveryMiddleReturn =
        deliveryWay === "STORE_TO_CUSTOMER" ? "STORE" : "CUSTOMER";
      return `GOT_BAG_${deliveryMiddleReturn}`;
    case "completed":
      const deliveryMayReturn =
        deliveryWay === "STORE_TO_CUSTOMER" ? "CUSTOMER" : "STORE";
      return `BAG_DELIVERED_${deliveryMayReturn}`;
  }
  return status;
}

/*
function statusDefined(payloadStatus: string, deliveryWay?: string) {
  switch (payloadStatus.toUpperCase()) {
    case "ACCEPTED":
      return `REACHED_${deliveryWay}`;
    case `REACHED_${deliveryWay}`:
      return `GOT_BAG_${deliveryWay}`;
    case `GOT_BAG_${deliveryWay}`:
      return `BAG_PICKED_${deliveryWay}`;
    case `BAG_PICKED_${deliveryWay}`:
      return `BAG_PICKED_${deliveryWay}`;
  }
  return "ACCEPTED";
}
*/

