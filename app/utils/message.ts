import { ResponseVO } from '../model/vo/responseVo';

enum StatusCode {
  success = 200,
  badRequest = 400,
  unauthorized = 401,
  notFound = 404,
  forbiden = 403,
  unprocessableEntity = 422,
  internalServerError = 500,
  badGateway = 502,
  gatewayTimeout = 504
}

class Result {
  private statusCode: number;
  private code: number;
  private message: string;
  private data?: any;

  constructor(statusCode: number, code: number, message: string, data?: any) {
    this.statusCode = statusCode;
    this.code = code;
    this.message = message;
    this.data = data;
  }

  /**
   * Serverless: According to the API Gateway specs, the body content must be stringified
   */
  bodyToString () {
    return {
      statusCode: this.statusCode,
      body: JSON.stringify({
        code: this.code,
        message: this.message,
        data: this.data,
      }),
    };
  }
}

export class MessageUtil {
  static success(data: object): ResponseVO {
    const result = new Result(StatusCode.success, 200, 'success', data);

    return result.bodyToString();
  }

  static error(code: number = 500, message: string) {
    const result = new Result(code, code, message);

    return result.bodyToString();
  }
}
