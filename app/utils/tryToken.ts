import jsonwebtoken from 'jsonwebtoken';

export const getTryApiToken = (): string => {
  const data = {
    isDriver: true,
    scope: "DRIVER"
  }

  console.log('token tryIsSecure ->',jsonwebtoken.sign(data, 'tryIsSecure'))
  return jsonwebtoken.sign(data, 'driverIsSecure');
}

export const verifyTryApiToken = async (token: string): Promise<boolean> => {
  const data = {
      isTry: true,
      scope: "TRY_DELIVERY"
    }

  try {
    let decoded: any = await jsonwebtoken.verify(token, 'tryIsSecure');

    if(decoded.isTry && decoded.scope === data.scope){
      return true
    }else{
      return false
    }
  } catch (error) {
    console.log('[error] - verifyTryApiToken', error)
    return false
  }
}