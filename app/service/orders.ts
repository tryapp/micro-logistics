import { Model } from "mongoose";
import { SQS } from "aws-sdk";
import { CreateOrderDTO } from "../model/dto/createOrderDTO";
//import orderFactoryTry from './factory/orderFactoryTry';
//import orderFactoryClickEntregas from './factory/orderFactoryClickEntregas';
import OrderFactoryLalaMove from "./factory/orderFactoryLalaMove";
import axios from "axios";
import { OrdersDocument } from "../model/orders";
import { getTryApiToken } from "../utils/tryToken";
import getDriverOrder from "./external/clickEntregas/methods/getDriverOrder";
import getDriverOrderlalaMove from "./external/lalaMove/methods/getDriverOrder";
import { getOrderNextStatus } from "../utils/tryOrderStatusManager";
import { orderType, vehicleTypes } from "../service/external/lalaMove/utils";

/*
const SDKClient = require("@lalamove/lalamove-js");
const lalamoveSDKClient = new SDKClient.ClientModule(
  new SDKClient.Config(
    "pk_test_6d278ae9b7e2b92b8a7d62a18381834e",
    "sk_test_KSvI/isz+ClQhGVjDwtScGdWYj1+95b/D4FLnNGRSSfcGDN7UJbN+k4rQ4UjO4M8",
    "sandbox"
  )
);
*/

const buildParams = (params: object): object => {
  return {
    type: orderType.standard,
    matter: getMatterText(),
    vehicle_type_id: vehicleTypes.motorbike,
    total_weight_kg: 4,
    is_client_notification_enabled: true,
    points: getPointsParams(params as OrdersDocument),
  };
};

const getMatterText = (): string => {
  return "Recomendação do app Try";
};

const getPointsParams = (params: OrdersDocument): Array<object> => {
  const isStoreToCustomer = params.tryOrderType === "STORE_TO_CUSTOMER";
  const notes = {
    customer: "Coletar bag de " + params.tryCustomerFirstName,
    store:
      "Loja " +
      params.tryStoreName +
      "/ Coletar pedido #" +
      params.tryOrderNumber +
      ".",
  };
  return [
    {
      address: params.addressFrom,
      apartment_number: params.addressFromComplement,
      contact_person: {
        phone: isStoreToCustomer
          ? params.tryStorePhone
          : params.tryCustomerPhone,
        name: isStoreToCustomer
          ? params.tryStoreName
          : params.tryCustomerFirstName,
      },
      note: notes[isStoreToCustomer ? "store" : "customer"],
    },
    {
      address: params.addressTo,
      apartment_number: params.addressToComplement,
      contact_person: {
        phone: isStoreToCustomer
          ? params.tryCustomerPhone
          : params.tryStorePhone,
        name: isStoreToCustomer
          ? params.tryCustomerFirstName
          : params.tryStoreName,
      },
      note: notes[isStoreToCustomer ? "customer" : "store"],
    },
  ];
};

export class OrdersService {
  private orders: Model<any>;
  constructor(orders: Model<any>) {
    this.orders = orders;
  }

  /**
   * Create order
   * @param params
   */
  protected async createOrder(params: CreateOrderDTO): Promise<object> {
    const dataRequest = buildParams(params);
    await axios
      .post(`${process.env.TRY_SERVICE_API}/api/v1/deliveryOrders`, dataRequest)
      .then(async (response) => {
        return response.data;
      })
      .catch((err) => {
        return err;
      });

    try {
      const result = await this.orders.create({
        addressFrom: params.addressFrom,
        addressFromComplement: params.addressFromComplement,
        addressFromLat: params.addressFromLat,
        addressFromLong: params.addressFromLong,
        addressTo: params.addressTo,
        addressToComplement: params.addressToComplement,
        addressToLat: params.addressToLat,
        addressToLong: params.addressToLong,
        number: params.number,
        orderRequestId: params.orderRequestId,
        tryOrderTotalAmount: params.tryOrderTotalAmount,
        tryOrderPackingListTotalAmount: params.tryOrderPackingListTotalAmount,
        tryStoreId: params.tryStoreId,
        tryOrderNumber: params.tryOrderNumber, //numero da order
        tryOrderType: params.tryOrderType,
        tryCustomerFirstName: params.tryCustomerFirstName,
        tryStoreName: params.tryStoreName,
        tryCustomerPhone: params.tryCustomerPhone,
        tryStorePhone: params.tryStorePhone,
        deliverOrderStatus: "ACCEPTED",
        deliverDeliveryStatus: "ACCEPTED",
        status: "created",
      });

      await new OrderFactoryLalaMove().factoryMethod(result._id);

      //await new orderFactoryClickEntregas().factoryMethod(result._id)
      // if(result.tryOrderTotalAmount > 1000){
      //   await new orderFactoryClickEntregas().factoryMethod(result._id)
      // }else{
      //   await new orderFactoryTry().factoryMethod(result._id)
      // }

      return result;
    } catch (err) {
      console.error(err);

      throw err;
    }
  }

  /**
   * Update a order by id
   * @param id
   * @param data
   */
  protected async updateOrders(_id: string, data: any) {
    console.log("ID ---->", _id);
    console.log("DATA -->", data);

    try {
      const order = await this.orders.findOne({ deliverOrderId: _id });

      data.deliverOrderStatus = data.status;

      const result = await this.orders.findOneAndUpdate(
        { deliverOrderId: _id },
        { $set: data },
        { new: true }
      );

      const newStatus = getOrderNextStatus(
        data.deliverOrderStatus,
        order.deliverOrderStatus,
        result.tryOrderType
      );

      if (newStatus) {
        console.log(
          `WILL CALL PLATFORM: ${process.env.TRY_PLATFORM_API}/driver/v3/order`,
          newStatus
        );

        const headerOptions = {
          headers: { Authorization: `Bearer ${getTryApiToken()}` },
        };
        const bodyOptions = {
          orderRequestId: result._id,
          status: newStatus,
          driver: {
            name: result.driverName,
            phoneNumber: {
              phoneNo: result.driverPhone,
              countryCode: "55",
              ISO: "BR",
            },
            profilePic: {
              original: result.driverPhotoUrl,
              thumbnail: result.driverPhotoUrl,
              fileName: "deliveryman-003-512.png",
              type: "image/png",
              thumbnailMed: result.driverPhotoUrl,
              processed: "string",
              _id: "string",
            },
            lat: result.driverLatitude,
            long: result.driverLongitude,
          },
        };
        // enable when integration works
        await axios.put(
          `${process.env.TRY_PLATFORM_API}/driver/v3/order/${result._id}`,
          bodyOptions,
          headerOptions
        );
      }

      return result;
    } catch (err) {
      console.error(err);

      throw err;
    }
  }

  async updateOrdersLalaMove(_id: string, data: any) {
    console.log("ID ------>", _id);
    console.log("DATA ---->", data);

    try {
      const order = await this.orders.findOne({ deliverOrderId: _id });

      data.deliverOrderStatus = data.status;

      const result = await this.orders.findOneAndUpdate(
        { deliverOrderId: _id },
        { $set: data },
        { new: true }
      );

      const newStatus = getOrderNextStatus(
        data.deliverOrderStatus,
        order.deliverOrderStatus,
        result.tryOrderType
      );

      if (newStatus) {
        console.log(
          `WILL CALL PLATFORM: ${process.env.TRY_PLATFORM_API}/driver/v3/order`,
          newStatus
        );

        const headerOptions = {
          headers: { Authorization: `Bearer ${getTryApiToken()}` },
        };
        const bodyOptions = {
          orderRequestId: result._id,
          status: newStatus,
          driver: {
            name: result.driverName,
            phoneNumber: {
              phoneNo: result.driverPhone,
              countryCode: "55",
              ISO: "BR",
            },
            profilePic: {
              original: result.driverPhotoUrl,
              thumbnail: result.driverPhotoUrl,
              fileName: "deliveryman-003-512.png",
              type: "image/png",
              thumbnailMed: result.driverPhotoUrl,
              processed: "string",
              _id: "string",
            },
            lat: result.driverLatitude,
            long: result.driverLongitude,
          },
        };

        // enable when integration works
        await axios.put(
          `${process.env.TRY_PLATFORM_API}/driver/v3/order`,
          bodyOptions,
          headerOptions
        );
      }
      return result;
    } catch (err) {
      console.error(err);

      throw err;
    }
  }

  /**
   * Find orders
   */
  protected findOrders() {
    return this.orders.find();
  }

  /**
   * Query order by id
   * @param id
   */
  protected findOneOrderById(_id: string) {
    return this.orders.findById(_id);
  }

  /**
   * Query driver order by id
   * @param id
   */
  protected async findDriverIndfoByOrderId(_id: string) {
    let order: OrdersDocument = await this.orders.findById(_id);
    console.log('ORDER ->',order);
    console.log('ID ->',_id);
    await this.orders.findOne(
      {},
      {},
      { sort: { createdAt: -1 } },
      (err, data) => {
        console.log("dataAllOrders ->", data);
        console.log("err_dataAllOrders ->", err);
      }
    );
    if (order.companyName === "Try Service Drivers") {
      return axios.get(
        `${process.env.TRY_SERVICE_API}/api/v1/deliveryOrders/drivers/${order.deliverOrderId}`
      );
    } else if (order.companyName === "ClickEntregas") {
      return new getDriverOrder(order).process();
    } else if (order.companyName === "LalaMove") {
      return new getDriverOrderlalaMove(order).process();
    } else {
      throw new Error("no service found");
    }
  }

  /**
   * Enqueue order to service
   */
  protected enqueueOrder(params) {
    const sqs = new SQS();
    const queueUrl: string = process.env.AWS_SQS_QUEUE;

    return sqs
      .sendMessage({
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify({ price: params.deliverTotalAmount }),
        MessageAttributes: {
          AttributeNameHere: {
            StringValue: params._id.toString(),
            DataType: "String",
          },
        },
      })
      .promise();
  }
}
