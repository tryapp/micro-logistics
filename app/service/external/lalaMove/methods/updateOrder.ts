import axios from "axios";
import { getTryApiToken } from '../../../../utils/tryToken';
import { orders } from "../../../../model/orders";
import { UpdateOrderRequest, UpdateOrderRequestData, UpdateOrderDeliveryRequestData  } from "../config/types";
import { getOrderNextStatus } from "../../../../utils/tryOrderStatusManager";

export default class UpdateOrder {
  private options: UpdateOrderRequest;

  constructor(options){
    this.options = options
  }

  public process = async () => {
    console.log('OPTIONS -->', this.options)
    console.log('OPTIONS ORDER -->', this.options.order)
    console.log('OPTIONS DELIVERY -->', this.options.delivery)

    return orders.findOneAndUpdate(
     // { _id: order._id },
      { 
        $push: {apiData: this.options} 
      },
      { new: true },
    )
    /*
        if(this.options){
      console.log("---> 1 [UPDATE ORDER] processing data", JSON.stringify(this.options.order))

      return this.processOrderData();
    }else if(this.options.delivery){
      console.log("--->  2 [UPDATE DELIVERY] processing data", JSON.stringify(this.options.delivery))

      return this.processDeliveryData();
    }else{
      console.log("---> 3 [ERROR] No content available", JSON.stringify(this.options))
      
      throw new Error("No content available");
    }
    */


  }

}