//import axios from 'axios';
import { orders, OrdersDocument } from "../../../../model/orders";
//import { getTryApiToken } from "../../../../utils/tryToken";

const SDKClient = require("@lalamove/lalamove-js");
const lalamoveSDKClient = new SDKClient.ClientModule(
  new SDKClient.Config(
    "pk_test_6d278ae9b7e2b92b8a7d62a18381834e",
    "sk_test_KSvI/isz+ClQhGVjDwtScGdWYj1+95b/D4FLnNGRSSfcGDN7UJbN+k4rQ4UjO4M8",
    "sandbox"
  )
);
export default class GetDriverOrderLalaMove {
  private order: OrdersDocument;

  constructor(order: object){
    this.order = order as OrdersDocument 
  }

  public process = async () => {
    console.log("findDriverIndfoByOrderId ->", this.order);

    ///aqui entra a solicitação de onde está o driver
    console.log('this.order', this.order);
      const dadosOrder = await lalamoveSDKClient.Order.retrieve(
        "BR",
        this.order.deliverOrderId
      );
      console.log('dadosOrder', JSON.stringify(dadosOrder))
      const driverInfo = await lalamoveSDKClient.Driver.retrieve(
        "BR",
        dadosOrder.driverId,
        this.order.deliverOrderId
      );
      console.log('DRIVER ->', driverInfo);

    const order_lalamove = this.order.deliverOrderId;
    console.log('order_lalamove_new', order_lalamove);
    
    await this.updateDriverInfo(driverInfo)
    const resultDriverSdk = { 
      data:{
        is_successful: true, 
        courier: { 
          courier_id: driverInfo.id, 
          surname: driverInfo.contact.name, 
          name: driverInfo.contact.name, 
          middlename: null, 
          phone: driverInfo.contact.phone, 
          photo_url: driverInfo.photo, 
          latitude: driverInfo.coordinates.lat, 
          longitude: driverInfo.coordinates.lng,
      } 
      }
      
  }
    console.log(`result getDriverOrder -> ${JSON.stringify(resultDriverSdk)}`)

    return resultDriverSdk;
}
  private updateDriverInfo = (result) => {
    
    return orders.findOneAndUpdate(
      { _id: this.order._id },
      { $set: {
        driverName: result.contact.name,
          driverLatitude: result.coordinates.lat,
          driverLongitude: result.coordinates.lng,
          driverPhotoUrl: result.photo,
          driverPhone: result.contact.phone
        }
      },
      { new: true },
    )
  }


  /*

    let driverData = {
    surname: responseData.surname,
    name: responseData.name,
    middlename: responseData.middlename,
    phone: responseData.phone,
    photo_url: responseData.photo_url,
    driverCurrentLat: responseData.latitude,
    driverCurrentLong: responseData.longitude,
  };
  */

  
}