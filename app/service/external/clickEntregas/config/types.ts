// create

export type apiCreateResponse = {
  is_successful: boolean,
  order?: createOrderResponse
}

export type createOrderResponse = {
  type: string,
  order_id: number,
  order_name: string,
  vehicle_type_id: number,
  created_datetime: Date,
  finish_datetime?: Date,
  status: string,
  status_description: string,
  matter: string,
  total_weight_kg?: number,
  is_client_notification_enabled: boolean,
  is_contact_person_notification_enabled: boolean,
  loaders_count?: number,
  backpayment_details?: string,
  payment_amount: number, 
  delivery_fee_amount?: number, 
  weight_fee_amount?: number, 
  insurance_amount?: number, 
  insurance_fee_amount?: number, 
  loading_fee_amount?: number, 
  money_transfer_fee_amount?: number, 
  suburban_delivery_fee_amount?: number, 
  overnight_fee_amount?: number, 
  discount_amount?: number, 
  cod_fee_amount?: number, 
  backpayment_photo_url?: string,
  itinerary_document_url?: string,
  waybill_document_url?: string,
  is_motobox_required: boolean, 
  payment_method: string,
  bank_card_id?: number,
  courier?: drivrerOrderResponse,
  points: Array<createOrderPointResponse>
}

type createOrderPointResponse = {
  point_id?: number,
  delivery_id?: number,
  client_order_id?: number,
  address: string,
  latitude: string,
  longitude: string,
  required_start_datetime: Date,
  required_finish_datetime?: Date,
  arrival_start_datetime?: Date,
  arrival_finish_datetime?: Date,
  estimated_arrival_datetime?: Date,
  courier_visit_datetime?: Date,
  contact_person: { name?: string, phone?: string },
  taking_amount: string,
  buyout_amount: string,
  note?: string,
  packages: Array<object>,
  is_cod_cash_voucher_required: boolean,
  is_order_payment_here: boolean,
  building_number?: number,
  entrance_number?: number,
  intercom_code?: string,
  floor_number?: number,
  apartment_number?: number,
  invisible_mile_navigation_instructions?: string,
  place_photo_url?: string,
  sign_photo_url?: string,
  checkin?: { recipient_full_name: string, recipient_position?: string},
  tracking_url?: string,
}

// update

export type UpdateOrderRequest = { 
    event_datetime: Date, 
    event_type: string, 
    order?: UpdateOrderRequestData,
    delivery?: UpdateOrderDeliveryRequestData
}

// update order

export type UpdateOrderRequestData = { 
  type: string, 
  order_id: number, 
  order_name: string, 
  vehicle_type_id: number, 
  created_datetime: Date, 
  finish_datetime?: Date, 
  status: string, 
  status_description: string, 
  matter: string, 
  total_weight_kg: number, 
  is_client_notification_enabled: boolean, 
  is_contact_person_notification_enabled: boolean, 
  loaders_count: number, 
  backpayment_details?: string, 
  points: Array<UpdateOrderRequestDataPoint>, 
  payment_amount: number, 
  delivery_fee_amount: number, 
  weight_fee_amount: number, 
  insurance_amount: number, 
  insurance_fee_amount: number, 
  loading_fee_amount: number, 
  money_transfer_fee_amount: number, 
  suburban_delivery_fee_amount: number, 
  overnight_fee_amount: number, 
  discount_amount: number, 
  backpayment_amount: number, 
  cod_fee_amount: number, 
  backpayment_photo_url: null, 
  itinerary_document_url: null, 
  waybill_document_url: null, 
  is_motobox_required: boolean, 
  payment_method: string, 
  bank_card_id: null,
  courier?: drivrerOrderResponse
} 

export type UpdateOrderRequestDataPoint = { 
  point_id?: number, 
  delivery_id?: number, 
  client_order_id?: number, 
  address: string, 
  latitude: string, 
  longitude: string, 
  required_start_datetime: Date, 
  required_finish_datetime: Date, 
  arrival_start_datetime?: Date, 
  arrival_finish_datetime?: Date, 
  courier_visit_datetime?: Date, 
  contact_person: { 
      name?: string, 
      phone?: string 
  }, 
  taking_amount: string, 
  buyout_amount: string, 
  note?: string, 
  packages: Array<object>, 
  is_cod_cash_voucher_required: boolean, 
  is_order_payment_here: boolean, 
  building_number?: number, 
  entrance_number?: number, 
  intercom_code?: string, 
  floor_number?: number, 
  apartment_number?: string, 
  invisible_mile_navigation_instructions?: string, 
  place_photo_url?: string, 
  sign_photo_url?: string, 
  checkin: { 
      recipient_full_name: string, 
      recipient_position?: string 
  }, 
  tracking_url?: string 
}

// update delivery

export type UpdateOrderDeliveryRequestData = { 
    delivery_id: number, 
    delivery_type: string, 
    order_id: number, 
    client_id: number, 
    client_order_id?: number, 
    address: string, 
    status: string, 
    status_datetime: Date, 
    created_datetime: Date, 
    order_name: string, 
    order_payment_amount: number, 
    delivery_price_amount: number, 
    point_id: number, 
    contact_person: { 
        name?: string, 
        phone: string
    }, 
    note?: string, 
    building_number?: number, 
    apartment_number?: number, 
    entrance_number?: number, 
    intercom_code?: string, 
    floor_number?: number, 
    invisible_mile_navigation_instructions?: string, 
    required_start_datetime?: Date, 
    required_finish_datetime?: Date, 
    taking_amount: number, 
    buyout_amount: number, 
    is_cod_cash_voucher_required: boolean, 
    is_motobox_required: boolean, 
    is_door_to_door: boolean, 
    is_return_to_first_point_required: boolean, 
    matter?: string, 
    insurance_amount: number, 
    weight_kg: number, 
    packages: Array<object>, 
    checkin_issue_name: string,
    courier?: drivrerOrderResponse 
} 

// util

export type apiDriverResponse = {
  is_successful: boolean,
  courier?: drivrerOrderResponse
}

type drivrerOrderResponse = {
  courier_id: number,
  surname: string,
  name: string,
  middlename: string,
  phone: string,
  photo_url: string,
  latitude: string,
  longitude: string,
}