import axios from 'axios';
import { orders, OrdersDocument } from "../../../../model/orders";

export default class GetDriverOrder {
  private order: OrdersDocument;

  constructor(order: object){
    this.order = order as OrdersDocument 
  }

  public process = async () => {
    const headers = {headers: {'X-DV-Auth-Token': process.env.CLICKENTREGAS_TOKEN}}

    const result = await axios.get(`${process.env.CLICKENTREGAS_API}/api/business/1.1/courier?order_id=${this.order.deliverOrderId}`, headers)

    await this.updateDriverInfo(result.data)

    if(result.data.courier){
      result.data.driver = result.data.courier
      
      delete result.data.courier
    }

    return result
  }

  private updateDriverInfo = (result) => {
    return orders.findOneAndUpdate(
      { _id: this.order._id },
      { $set: {
        driverName: result.contact.name,
          driverLatitude: result.coordinates.lat,
          driverLongitude: result.coordinates.lng,
          driverPhotoUrl: result.photo,
          driverPhone: result.contact.phone
        }
      },
      { new: true },
    )
  }
}