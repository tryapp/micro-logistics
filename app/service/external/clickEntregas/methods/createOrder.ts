import axios from 'axios';
import { OrdersDocument } from '../../../../model/orders';

import { orderType, vehicleTypes  } from '../utils';
 
const buildParams = (params: object): object => {
  return {
    type: orderType.standard,
    matter: getMatterText(),
    vehicle_type_id: vehicleTypes.motorbike,
    total_weight_kg: 4,
    is_client_notification_enabled: true,
    points: getPointsParams(params as OrdersDocument),
  }
}

const getMatterText = (): string => {
  return "Recomendação do app Try"
}

const getPointsParams = (params: OrdersDocument): Array<object> => {
  const isStoreToCustomer = params.tryOrderType === "STORE_TO_CUSTOMER";
  const notes = {
    customer: 'Coletar bag de '+params.tryCustomerFirstName,
    store: 'Loja '+params.tryStoreName+'/ Coletar pedido #'+params.tryOrderNumber+'.',
  }

  return [{
    address: params.addressFrom,
    apartment_number: params.addressFromComplement,
    contact_person: {
      phone: isStoreToCustomer ? params.tryStorePhone : params.tryCustomerPhone,
      name: isStoreToCustomer ? params.tryStoreName : params.tryCustomerFirstName,
    },
    note: notes[isStoreToCustomer ? 'store' : 'customer']
  },{
    address: params.addressTo,
    apartment_number: params.addressToComplement,
    contact_person: {
      phone: isStoreToCustomer ? params.tryCustomerPhone : params.tryStorePhone,
      name: isStoreToCustomer ? params.tryCustomerFirstName : params.tryStoreName
    },
    note: notes[isStoreToCustomer ? 'customer' : 'store']
  }]
}

const createOrder = (params: object): Promise<object> => {
  const headers = {headers: {'X-DV-Auth-Token': process.env.CLICKENTREGAS_TOKEN}}
  
  const dataRequest = buildParams(params)

  console.log('---> dataRequest', dataRequest)

  return axios.post(`${process.env.CLICKENTREGAS_API}/api/business/1.1/create-order`, dataRequest , headers)
}

export default createOrder;