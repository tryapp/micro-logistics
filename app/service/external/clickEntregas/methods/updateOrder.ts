import axios from "axios";
import { getTryApiToken } from '../../../../utils/tryToken';
import { orders } from "../../../../model/orders";
import { UpdateOrderRequest, UpdateOrderRequestData, UpdateOrderDeliveryRequestData  } from "../config/types";
import { getOrderNextStatus } from "../../../../utils/tryOrderStatusManager";

export default class UpdateOrder {
  private options: UpdateOrderRequest;

  constructor(options){
    this.options = options
  }

  public process = async () => {
    if(this.options.order){
      console.log("---> [UPDATE ORDER] processing data", JSON.stringify(this.options.order))

      return this.processOrderData();
    }else if(this.options.delivery){
      console.log("---> [UPDATE DELIVERY] processing data", JSON.stringify(this.options.delivery))

      return this.processDeliveryData();
    }else{
      console.log("---> [ERROR] No content available", JSON.stringify(this.options))
      
      throw new Error("No content available");
    }
  }

  private processOrderData = async () => {
    const data: UpdateOrderRequestData = this.options.order;

    const order = await this.getOrderByOrderId(data.order_id)

    if(!order){
      console.log(`---> [PROCESS ORDER DATA] order NOT FOUND`, data.order_id)

      throw new Error(`order ${data.order_id} not found`);
    }

    console.log(`---> [PROCESS ORDER DATA] order (${data.order_id}) found`, order)

    const orderParams = {
      deliverOrderStatus: data.status,
      finishedAt: data.finish_datetime,
      addressFromRecipientName: data.points[0].checkin && data.points[0].checkin.recipient_full_name,
      addressFromRecipientPosition: data.points[0].checkin && data.points[0].checkin.recipient_position,
      addressFromPlacePhoto: data.points[0].place_photo_url,
      addressFromSignPhoto: data.points[0].sign_photo_url,
      addressToRecipientName: data.points[1].checkin && data.points[1].checkin.recipient_full_name,
      addressToRecipientPosition: data.points[1].checkin && data.points[1].checkin.recipient_position,
      addressToPlacePhoto: data.points[1].place_photo_url,
      addressToSignPhoto: data.points[1].sign_photo_url,
      driverName: `${data.courier && data.courier.name} ${data.courier && data.courier.surname}`,
      driverLatitude: data.courier && data.courier.latitude,
      driverLongitude: data.courier && data.courier.longitude,
      driverPhotoUrl: data.courier && data.courier.photo_url,
      driverPhone: data.courier && data.courier.phone
    }

    console.log('---> [PROCESS ORDER DATA] order params', JSON.stringify(orderParams))
    
    return this.processUpdateData(order, orderParams)
  }

  private processDeliveryData = async () => {
    const data: UpdateOrderDeliveryRequestData = this.options.delivery;

    const order = await this.getOrderByOrderId(data.order_id)

    if(!order){
      console.log(`---> [PROCESS DELIVERY DATA] order NOT FOUND`, data.order_id)

      throw new Error(`order ${data.order_id} not found`);
    }

    const orderParams = {
      deliverDeliveryStatus: data.status,
      updatedAt: data.status_datetime,
      driverName: `${data.courier && data.courier.name}` || `${data.courier && data.courier.surname}`,
      driverLatitude: order.driverLatitude || (data.courier && data.courier.latitude),
      driverLongitude: order.driverLongitude || (data.courier && data.courier.longitude),
      driverPhotoUrl: order.driverPhotoUrl || (data.courier && data.courier.photo_url),
      driverPhone: order.driverPhone || (data.courier && data.courier.phone)

    }

    console.log('---> [PROCESS DELIVERY DATA] delivery params', JSON.stringify(orderParams))

    return this.processUpdateData(order, orderParams)
  }

  private processUpdateData = async (order, data) => {
    const nextTryOrderStatus = getOrderNextStatus(data.deliverDeliveryStatus, order.deliverDeliveryStatus, order.tryOrderType)

    console.log('---> [ORDER]', JSON.stringify(order))
    console.log('---> [DATA]', JSON.stringify(data))
    console.log('---> [PROCESS UPDATE DATA] data.deliverDeliveryStatus is', data.deliverDeliveryStatus)
    console.log('---> [PROCESS UPDATE DATA] order.deliverDeliveryStatus is', order.deliverDeliveryStatus)
    console.log('---> [PROCESS UPDATE DATA] nextTryOrderStatus is', nextTryOrderStatus)

    try {
      order = await this.updateOrder(order, data)

      console.log('---> [PROCESS UPDATE DATA] order updated', order)

      if(this.isOrderAvailable(order) && nextTryOrderStatus !== undefined) {
        if(process.env.NODE_ENV === 'local'){
          console.log(`--> [PROCESS UPDATE DATA] call try platform: ${process.env.TRY_PLATFORM_API}/driver/v3/order`, nextTryOrderStatus)
        }else{
          console.log(`--> [PROCESS UPDATE DATA] call try platform: ${process.env.TRY_PLATFORM_API}/driver/v3/order`, nextTryOrderStatus)
          
          const tryResponse = await this.updateOrderOnTryPlatform(order, nextTryOrderStatus)

          console.log(`--> [PROCESS UPDATE DATA] response from try platform`, tryResponse)
        }
      }else{
        console.log(`---> [PROCESS UPDATE DATA] no need to call try platform status = ${data.status} ,deliverDeliveryStatus = ${data.deliverDeliveryStatus}`)
      }
    } catch (error) {
      console.log(`--> [ERROR PROCESS UPDATE DATA]`, error.stack || error)

      throw new Error(error.message);
    }
  }

  private getOrderByOrderId = (order_id: number) => {
    return orders.findOne({ deliverOrderId: order_id.toString() })
  }

  private updateOrder = async (order, orderParams) => {
    return orders.findOneAndUpdate(
      { _id: order._id },
      { $set: orderParams, 
        $push: {apiData: this.options} 
      },
      { new: true },
    )
  }

  private updateOrderOnTryPlatform = (order: any, nextTryOrderStatus: string): any => {
    const headerOptions = {headers: { Authorization: `Bearer ${getTryApiToken()}`}}
    const bodyOptions = {
      "orderRequestId": order._id,
      "status": nextTryOrderStatus,
      "driver": {
        "name": order.driverName,
        "phoneNumber": {
          "phoneNo": order.driverPhone,
          "countryCode": "55",
          "ISO": "BR"
        },
        "profilePic": {
          "original": order.driverPhotoUrl,
          "thumbnail": order.driverPhotoUrl,
          "fileName": "deliveryman-003-512.png",
          "type": "image/png",
          "thumbnailMed": order.driverPhotoUrl,
          "processed": "string",
          "_id": "string"
        },
        "lat": order.driverLatitude,
        "long": order.driverLongitude
      }
    }

    console.log(`--> [PROCESS UPDATE DATA] call try platform: headerOptions`, JSON.stringify(headerOptions))
    console.log(`--> [PROCESS UPDATE DATA] call try platform: bodyOptions`, JSON.stringify(bodyOptions))

    return axios.put(`${process.env.TRY_PLATFORM_API}/driver/v3/order`, bodyOptions, headerOptions);
  }

  private isOrderAvailable = (order): boolean => {
    return order.deliverOrderStatus === 'available' || order.deliverOrderStatus === 'active' || order.deliverOrderStatus === 'completed'
  }
}