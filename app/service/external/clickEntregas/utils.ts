export enum orderType {
  standard = 'standard',
  same_day = 'same_day'
}

export enum orderStatus {
  new = 'new',
  available = 'available',
  active = 'active',
  completed = 'completed',
  reactivated = 'reactivated',
  draft = 'draft',
  canceled = 'canceled',
  delayed = 'delayed'
}

export enum deliveryStatus {
  invalid = 'invalid',
  draft = 'draft',
  planned = 'planned',
  active = 'active',
  finished = 'finished',
  canceled = 'canceled',
  delayed = 'delayed',
  failed = 'failed',
  courier_assigned = 'courier_assigned',
  courier_departed = 'courier_departed',
  parcel_picked_up = 'parcel_picked_up',
  courier_arrived = 'courier_arrived',
  deleted = 'deleted'
}

export enum vehicleTypes {
  car = 7,
  motorbike = 8
}

export const apiErrors = {
  unexpected_error: "Unexpected error. Please let us know at api@clickentregas.com.",
  invalid_api_version: "Unknown API version. Available versions are 1.0 and 1.1.",
  required_api_upgrade: "Requested API version was discontinued. You should use the latest version instead.",
  requests_limit_exceeded: "You have reached an API usage limit. Limits are: 100 requests per minute, 5 000 requests per 24 hours.",
  required_auth_token: "X-DV-Auth-Token header is missing from the request.",
  invalid_auth_token: "X-DV-Auth-Token you are sending is invalid.",
  required_method_get: "HTTP method GET is required.",
  required_method_post: "HTTP method POST is required.",
  invalid_post_json: "POST request body must be in JSON format.",
  invalid_parameters: "Request parameters contain errors. Look at parameter_errors response field for details.",
  unapproved_contract: "Your agreement is not approved yet (for legal entities).",
  service_unavailable: "Our service is temporarily unavailable. Please try again later.",
  invalid_api_method: "Unknown API method was requested.",
  buyout_not_allowed: "You do not have access to buyout feature.",
  order_cannot_be_edited: "Order cannot be edited.",
  order_cannot_be_canceled: "Order cannot be canceled.",
  insufficient_balance: "Your balance is too low (for legal entities).",
  buyout_amount_limit_exceeded: "Total buyout amount in your active orders is too large. You do not have sufficient balance / credit limit to place the new order.",
  route_not_found: "Route not found.",
  total_payment_amount_limit_exceeded: "Exceeded maximum order price.",
  order_is_duplicate: "Duplicate order rejected",
  insufficient_funds: "Insufficient funds on your bank card",
  card_payment_failed: "Bank card payment failed"
}