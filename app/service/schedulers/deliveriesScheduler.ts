import axios from "axios";
import { orders, OrdersDocument  } from "../../model/orders";
import { ETAParams, getETA } from "../../utils/eta";
import GetDriverOrder from "../../service/external/clickEntregas/methods/getDriverOrder";
import GetDriverOrderLala from "../../service/external/lalaMove/methods/getDriverOrder";
import { getTryApiToken } from '../../utils/tryToken';

export default class DeliveriesScheduler {

  public async process() {
    let activeOrders = await this.getActiveOrders()
    console.log('---> activeOrders total is == ', activeOrders.length);
    console.log('---> activeOrders == ', activeOrders);
    
    const promises = activeOrders.map(async (order) => {
      console.log(`---> process order [${order._id}] try order number [${order.tryOrderNumber}]`);

      if(!this.isOrderAlreadyNotified(order)){
        order.tryOrderType === "STORE_TO_CUSTOMER"
        ? await this.processStoreToCustomer(order)
        : await this.processCustomerToStore(order)
      }
    })

    await Promise.all(promises)
  }

  private async processStoreToCustomer(order: OrdersDocument): Promise<void>{
    if(
        (order.deliverOrderStatus === 'active' && order.deliverDeliveryStatus === 'active') 
        && await this.isETALess5minutes(order)
      ){
        console.log(`---> try order number [${order.tryOrderNumber}] is less than 5 minutes to arrive`);

        await this.callTryApi(order)
    }else{
      console.log(`---> try order number [${order.tryOrderNumber}] is NOT less than 5 minutes to arrive`);
      console.log(`---> deliverOrderStatus: ${order.deliverOrderStatus}`)
      console.log(`---> deliverDeliveryStatus: ${order.deliverDeliveryStatus}`)
    }
  }

  private async processCustomerToStore(order: OrdersDocument): Promise<void>{
    if(
        (order.deliverOrderStatus === 'active' && order.deliverDeliveryStatus === 'courier_departed') 
        && await this.isETALess5minutes(order)
      ){
        await this.callTryApi(order)
    }else{
      console.log(`---> try order number [${order.tryOrderNumber}] is NOT less than 5 minutes to arrive`);
      console.log(`---> deliverOrderStatus: ${order.deliverOrderStatus}`)
      console.log(`---> deliverDeliveryStatus: ${order.deliverDeliveryStatus}`)
    }
  }

  private async getActiveOrders(): Promise<Array<OrdersDocument>>{
    const activeOrders = await orders.find({deliverOrderStatus: { "$in": ["active", "courier_departed"]} })
    
    return activeOrders;
  }

  private async isETALess5minutes(order: OrdersDocument): Promise<Boolean>{
    const minutesToNotify = 5;
    const driverLocation = await this.getDriverLocation(order);
    const customerLocation = this.getCustomerLocation(order);
    
    const params: ETAParams = {
      origin: driverLocation,
      destination: customerLocation
    }

    console.log(`---> ETAParams [${JSON.stringify(params)}]`);

    const etaInMinutes = await getETA(params)

    console.log(`---> etaInMinutes [${etaInMinutes}]`);

    return etaInMinutes < minutesToNotify;
  }

  private async setOrderNotified(order: OrdersDocument): Promise<void>{
    await orders.findOneAndUpdate(
      {_id: order._id},
      { $set: {tryArrivalNotified: true} },
      { new: true }
    )
  }

  private isOrderAlreadyNotified(order: OrdersDocument): Boolean{
    console.log(`---> isOrderAlreadyNotified [${order.tryArrivalNotified}]`);

    return order.tryArrivalNotified
  }

  private getCustomerLocation(order: OrdersDocument): string{
    if(order.tryOrderType === 'STORE_TO_CUSTOMER'){
      return `${order.addressToLat},${order.addressToLong}`
    }else {
      return `${order.addressFromLat},${order.addressFromLong}`
    }
  }

  private async getDriverLocation(order: OrdersDocument): Promise<string>{
    if(order.companyName === 'ClickEntregas'){
      const driverLocation = await new GetDriverOrder(order).process()

      return `${driverLocation.data.driver.latitude || order.driverLatitude},${driverLocation.data.driver.longitude || order.driverLongitude}`
    }
    if(order.companyName === 'LalaMove'){
      const driverLocation = await new GetDriverOrderLala(order).process()
      console.log(">>>>>DRIVER LOCATION ",driverLocation)
      return `${driverLocation.data.courier.latitude || order.driverLatitude},${driverLocation.data.courier.longitude || order.driverLongitude}`
    }else{
      return new Promise<string>((resolve, _) => {
        resolve(`${order.driverLatitude},${order.driverLongitude}`)
      })
    }
  }

  private async callTryApi(order: OrdersDocument): Promise<void>{
    console.log(`---> setOrderNotified for order [${order._id}] try order number [${order.tryOrderNumber}]`);
    
    const headerOptions = {headers: { Authorization: `Bearer ${getTryApiToken()}`}}

    await axios.get(`${process.env.TRY_PLATFORM_API}/driver/v1/sendNearByNotficationToUser?orderNumber=${order.tryOrderNumber}`, headerOptions);
    
    await this.setOrderNotified(order)

    console.log(`---> callTryApi for order [${order._id}] try order number [${order.tryOrderNumber}]`);
  }
}