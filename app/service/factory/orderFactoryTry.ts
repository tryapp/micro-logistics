import { orders, OrdersDocument } from "../../model/orders";
import OrderFactory from "./orderFactory";
import axios from "axios";

export default class OrderFactoryClickEntregas extends OrderFactory {

  getCompanyName(): string {
    return 'Try Service Drivers';
  }
  
  async factoryMethod(id: string): Promise<object> {
    let order = await this.getOrder(id) as OrdersDocument;

    const resultApi = await this.createDeliveryOrderRequest(order);
    const resultApiJSON = resultApi.data

    return await orders.findOneAndUpdate(
        { _id: id },
        { $set: {
            companyName: this.getCompanyName(),
            deliverOrderId: resultApiJSON.order._id,
            deliverOrderNumber: resultApiJSON.order.orderNumber,
            deliverTotalAmount: resultApiJSON.order.payment_amount,
            status: resultApiJSON.order.status
          } 
        },
        { new: true },
      );
  }

  async createDeliveryOrderRequest(order: OrdersDocument) {
    let params: object = {
      pointA: order.addressFrom,
      pointALat: order.addressFromLat,
      pointALong: order.addressFromLong,
      pointB: order.addressTo,
      pointBLat: order.addressToLat,
      pointBLong: order.addressToLong,
      tryOrderNumber: order.tryOrderNumber
    }

    if(order.tryOrderType === "STORE_TO_CUSTOMER"){
      params = {
        pointAContactName: order.tryStoreName,
        pointAContactPhone: order.tryStorePhone,
        pointBContactName: order.tryCustomerFirstName,
        pointBContactPhone: order.tryCustomerPhone,
        ...params
      }
    }else{
      params = {
        pointAContactName: order.tryCustomerFirstName,
        pointAContactPhone: order.tryCustomerPhone,
        pointBContactName: order.tryStoreName,
        pointBContactPhone: order.tryStorePhone,
        ...params
      }
    }

    return axios.post(`${process.env.TRY_SERVICE_API}/api/v1/deliveryOrders`, params)
  }
}


