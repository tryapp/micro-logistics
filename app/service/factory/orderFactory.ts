import { orders } from "../../model/orders";

export default abstract class OrderFactory {
  /**
   * factoryMethod
   */
  public abstract factoryMethod(id: string): object;
  public abstract getCompanyName(): string;

  /**
   * getOrder
   */
  public getOrder(_id: string): object{
    return orders.findById(_id);
  }
}