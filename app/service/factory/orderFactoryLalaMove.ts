import { orders, OrdersDocument } from "../../model/orders";
import OrderFactory from "./orderFactory";
import axios from "axios";
import { MessageUtil } from "../../utils/message";
//import { getOrderNextStatusFactory } from "../../utils/tryOrderStatusManager";
import { getTryApiToken } from "../../utils/tryToken";

const buildParams = (params: any): object => {
  return {
    order_id: params.orderId,
    driverId: params.driverId,
    name: params.name,
    photo: params.photo,
    phone: params.phone,
  };
};

// TODO: adicionar no .env
const SDKClient = require("@lalamove/lalamove-js");
const lalamoveSDKClient = new SDKClient.ClientModule(
  new SDKClient.Config(



    "pk_test_6d278ae9b7e2b92b8a7d62a18381834e",
    "sk_test_KSvI/isz+ClQhGVjDwtScGdWYj1+95b/D4FLnNGRSSfcGDN7UJbN+k4rQ4UjO4M8",
    "sandbox"
    /*
    DB_URL="mongodb://prod-try-brasil:Z%2Fr2Gzbk%2BqQ!@54.235.138.194:27017/sampleDatabase?retryWrites=true&w=majority&authMechanism=DEFAULT&authSource=admin"

        "pk_prod_cb3ef10fad77d2e0c419f596cac56249",
    "sk_prod_49jxJCHHQ1ukJEU2O35s1/vcbGY/dcl5Rsir0J50qseNX1wvnczmIqhXaQHgjHMO",
    "production"

    */
  )
);

export default class OrderFactoryLalaMove extends OrderFactory {
  getCompanyName(): string {
    return "LalaMove";
  }

  async factoryMethod(id: string): Promise<object> {
    let order = (await this.getOrder(id)) as OrdersDocument;
    console.log(">>>>>>>>>>>>>> pedido", order);
    try {
      const resultApi = await this.createDeliveryOrderRequest(order);
      console.log("RESULT API >>>>>>>", resultApi)
      const objResultApi = JSON.stringify(resultApi);
      const converterResult = JSON.parse(objResultApi);
      console.log(">>>>>>>> convert result", converterResult)

      // Atualiza e seta a informação de companyName
      ///orders/{id}
      const payloadUpdate = {
        companyName: this.getCompanyName(),
        deliverOrderId: resultApi.id,
        deliverOrderNumber: order.tryOrderNumber,
        deliverTotalAmount: order.tryOrderTotalAmount,
        status: order.status,
      };


      await orders.findOneAndUpdate(
        { _id: order._id },
        {
          $set: {
            companyName: this.getCompanyName(),
            deliverOrderId: resultApi.id,
            deliverOrderNumber: payloadUpdate.deliverOrderNumber,
            deliverTotalAmount: payloadUpdate.deliverTotalAmount,
            status: payloadUpdate.status,
          },
        },
        { new: true }
      );
      await orders.findOneAndUpdate(
        { _id: order._id },
        {
          $set: {
            companyName: this.getCompanyName(),
            deliverOrderId: resultApi.id,
            deliverOrderNumber: payloadUpdate.deliverOrderNumber,
            deliverTotalAmount: payloadUpdate.deliverTotalAmount,
            status: payloadUpdate.status,
          },
        },
        { new: true }
      );


      // return MessageUtil.success(updateOrder);
    } catch (err) {
      console.log("Erro na atualização da order ->", err);
      return MessageUtil.error(
        200,
        `Ocorreu erro ao criar a order, \ndetalhes: ${err}`
      );
      // return MessageUtil.error(
      //   500,
      //   `Ocorreu erro ao criar a order, \ndetalhes: ${err}`
      // );
    }
  }

  async updateDeliveryOrderRequest(orderRefLalaMove, order) {
    try {
      console.log("ENTROU NO UPDATE  orderRefLalaMove ->" + orderRefLalaMove);
      console.log("ENTROU NO UPDATE  order ->" + JSON.stringify(order));
      await orders.findOneAndUpdate(
        { tryOrderNumber: order.metadata.tryOrderNumber },
        {
          $set: {
            status: order.status,
            deliverOrderStatus: 'available',
            deliverDeliveryStatus: order.status,
            driver: order.driver,
          },
        },
        { new: true }
      );
    } catch (err) {
      console.log("ERRO NO UPDATE");
      console.error(err);

      return MessageUtil.error(200, err.message);
      // return MessageUtil.error(err.code, err.message);
    }
  }

  async updateDeliveryOrderRequest2(orderRefLalaMove, order) {
    try {
      console.log("ENTROU NO UPDATE 2 orderRefLalaMove ->" + orderRefLalaMove);
      console.log("ENTROU NO UPDATE 2 order ->" + JSON.stringify(order));
      await orders.findOneAndUpdate(
        { tryOrderNumber: order.metadata.tryOrderNumber },
        {
          $set: {
            status: order.status,
            deliverOrderStatus: 'available',
            deliverDeliveryStatus: order.status,
            driver: order.driver,
          },
        },
        { new: true }
      );
    } catch (err) {
      console.log("ERRO NO UPDATE 2");
      console.error(err);
      return MessageUtil.error(200, err.message);
      // return MessageUtil.error(err.code, err.message);
    }
  }

  async createOrderAfterQuotation(quotation, order) {
    console.log("stops ->", quotation.stops[1].id);
    //console.log("stops ->", quotation.stops);
    let parcedPhone = "+" + order.tryCustomerPhone.replace(" ", "");
    const orderPayload = SDKClient.OrderPayloadBuilder.orderPayload()
      .withIsPODEnabled(true)
      .withQuotationID(quotation.id)
      .withSender({
        stopId: quotation.stops[0].id,
        name: order.tryCustomerFirstName,
        phone: parcedPhone,
      })
      .withRecipients([
        {
          stopId: quotation.stops[1].id,
          name: order.tryCustomerFirstName,
          phone: parcedPhone,
          remarks: "coletar o pedido " + order.tryOrderNumber + ". ENTREGA URGENTE! Restrição de horário. Necessário mochila vazia."
        },
      ])
      .withMetadata({
        tryOrderNumber: String(order.tryOrderNumber),
        //orderRequestId: String(order.orderRequestId),
        tryOrderType: order.tryOrderType,
      })
      .build();
    let orderLalamove = await lalamoveSDKClient.Order.create(
      'BR',
      orderPayload
    );
    console.log("Resultado da ordemLalaMove -> ", orderLalamove);

    if (!orderLalamove.id) {
      // return MessageUtil.error(500, "ERRO PARA CRIAR PEDIDO");
      return MessageUtil.success({ message: "ERRO PARA CRIAR PEDIDO" });
    }
    console.log(
      "passou pelo resultado da ordem da lalamove - > " +
      JSON.stringify(orderLalamove.id)
    );

    console.log(
      `coordinates -> ${JSON.stringify(orderLalamove.stops[0].coordinates)}`
    );
    await this.updateDeliveryOrderRequest(orderLalamove.id, orderPayload);
    //await this.updateDeliveryOrderRequest2(orderLalamove.id, orderPayload);

    await orders.findOneAndUpdate(
      { _id: order._id },
      {
        $set: {
          companyName: this.getCompanyName(),
          deliverOrderId: orderLalamove.orderId,
          deliverOrderNumber: orderLalamove.tryOrderNumber,
          deliverTotalAmount: orderLalamove.deliverTotalAmount,
          status: orderLalamove.status,
          deliverOrderStatus: 'available'
        },
      }
    );
    return orderLalamove;
  }

  async createDeliveryOrderRequest(order: OrdersDocument) {
    let params: object = {
      pointA: order.addressFrom + ', ' + order.number + ' - ' + order.addressFromComplement,
      //  pointALat: order.addressFromLat,
      //  pointALong: order.addressFromLong,
      pointB: order.addressTo + ', ' + order.number + ' - ' + order.addressToComplement,
      //  pointBLat: order.addressToLat,
      //  pointBLong: order.addressToLong,
      tryOrderNumber: order.tryOrderNumber,
    };

    if (order.tryOrderType === "STORE_TO_CUSTOMER") {
      params = {
        pointAContactName: order.tryStoreName,
        pointAContactPhone: order.tryStorePhone,
        pointBContactName: order.tryCustomerFirstName,
        pointBContactPhone: order.tryCustomerPhone,
        ...params,
      };
    } else {
      params = {
        pointAContactName: order.tryCustomerFirstName,
        pointAContactPhone: order.tryCustomerPhone,
        pointBContactName: order.tryStoreName,
        pointBContactPhone: order.tryStorePhone,
        ...params,
      };
    }

    /*  const co = {
        lat: order.addressFromLat,
        lng: order.addressFromLong,
      };
      const co2 = {
        lat: order.addressToLat,
        lng: order.addressToLong,
      };*/

    const stop1 = {
      //  coordinates: co,
      address: order.addressFrom + ', ' + order.number + ' - ' + order.addressFromComplement,
    };

    const stop2 = {
      //   coordinates: co2,
      address: order.addressTo + ', ' + order.number + ' - ' + order.addressToComplement,
    };

    // Aqui cria a quotation
    const quotationPayload =
      SDKClient.QuotationPayloadBuilder.quotationPayload()
        .withLanguage("pt_BR")
        .withServiceType("LALAGO")
        .withStops([stop1, stop2])
        .build();
    let quotation = await lalamoveSDKClient.Quotation.create(
      "BR",
      quotationPayload
    );
    if (!quotation.id) {
      //       return MessageUtil.error(500, "ERRO DE COTAÇÃO PARA PEDIDO");
      return MessageUtil.success({ message: "ERRO DE COTAÇÃO PARA PEDIDO" });
    }
    console.log(
      "passou pela COTAÇÃO PARA PEDIDO lalamove - > " +
      JSON.stringify(quotation.id)
    );

    return await this.createOrderAfterQuotation(quotation, order);
  }

  private getOrderByOrderId = (order_id: string) => {
    let teste = orders.findOne({ deliverOrderId: order_id });
    return teste;
  };

  async update(event: any, context?: any) {
    var events = JSON.parse(event.body);
    console.log(`Status-> ${events.eventType}`);


    //return MessageUtil.success(200,{ message: "ERRO vindo do lalamove" });
    if (!context) {
      // return MessageUtil.error(500, "ERRO DE COTAÇÃO PARA PEDIDO");
      return MessageUtil.success({ message: "ERRO ao buscar o evento" });
    }
    if (!events) {
      // return MessageUtil.error(500, "ERRO DE COTAÇÃO PARA PEDIDO");
      return MessageUtil.success({ message: "ERRO ao buscar o evento" });
    }
    if (events.eventType == "ORDER_STATUS_CHANGED") {

      /*

      

      */
      let tryResponse: any;
      console.log(tryResponse)
      let status_lalamove = events.data.order.status.toLowerCase();
      let order_lalamove = events.data.order;

      if (!order_lalamove) {
        // return MessageUtil.error(500, "ERRO DE COTAÇÃO PARA PEDIDO");
        return MessageUtil.success({ message: "ERRO vindo do lalamove" });
      }

      //let nextStatus;
      let order_db = {
        driverName: "",
        driverPhone: "",
        driverPhotoUrl: "",
        deliverOrderStatus: "",
        driverLat: "",
        driverLng: "",
        deliverDeliveryStatus: "",
        tryOrderNumber: "",
        OrderRequestId: ""
      };
      console.log("order_lalamove", order_lalamove);

      const dadosOrder = await lalamoveSDKClient.Order.retrieve(
        "BR",
        order_lalamove.orderId
      );
      console.log("DADOS ORDER", dadosOrder)
      if (!dadosOrder.driverId) {
        console.log("chegou aqui dadosOrder");
        // return MessageUtil.error(500, "ERRO DE COTAÇÃO PARA PEDIDO");
        return MessageUtil.success({ message: "ERRO DE DadosOrder" });
      }

      const driverInfo = await lalamoveSDKClient.Driver.retrieve(
        "BR",
        dadosOrder.driverId,
        order_lalamove.orderId
      );

      if (!driverInfo) {
        console.log("chegou aqui driverinfo");
        return MessageUtil.success({
          message: "ERRO ao pesquisar dados do motorista",
        });
      }
      if (!driverInfo.contact) {
        console.log("chegou aqui driverInfo.contact.name");
        return MessageUtil.success({
          message: "ERRO ao pesquisar dados do motorista",
        });
      }
      console.log("DRIVER ->", driverInfo);

      //const x = dadosOrder.id;
      order_db = await this.getOrderByOrderId(dadosOrder.id);
      console.log("apos a querry " + order_db);
      order_db.driverName = driverInfo.contact.name;
      order_db.driverPhone = driverInfo.contact.phone;
      order_db.driverPhotoUrl = driverInfo.photo;
      order_db.driverLat = driverInfo.coordinates.lat;
      order_db.driverLng = driverInfo.coordinates.lng;
      order_db.tryOrderNumber = order_db.tryOrderNumber;
      order_db.deliverOrderStatus = "available";

      if (status_lalamove == 'on_going') {
        tryResponse = this.updateOrderOnTryPlatform(order_db, "ACCEPTED");
        return MessageUtil.success({ message: "Order updated" });
      }

      if (status_lalamove == 'picked_up') {
        if (dadosOrder.metadata.tryOrderType == "STORE_TO_CUSTOMER") {


          /*
          tryResponse = await this.updateOrderOnTryPlatform(order_db, "REACHED_STORE");
          console.log('REACHED_STORE');

          tryResponse = await this.updateOrderOnTryPlatform(order_db, "GOT_BAG_STORE");
          console.log('GOT_BAG_STORE');
          */
          tryResponse = this.updateOrderOnTryPlatform(order_db, "BAG_PICKED_STORE");
          console.log("BAG_PICKED_STORE");
          return MessageUtil.success({ message: "Order updated" });

        }
        if (dadosOrder.metadata.tryOrderType == "CUSTOMER_TO_STORE") {
          /*
          tryResponse = await this.updateOrderOnTryPlatform(order_db, "REACHED_CUSTOMER");
          console.log('REACHED_CUSTOMER');

          tryResponse = await this.updateOrderOnTryPlatform(order_db, "GOT_BAG_CUSTOMER");
          console.log("GOT_BAG_CUSTOMER");
          */
          tryResponse = this.updateOrderOnTryPlatform(order_db, "BAG_PICKED_CUSTOMER");
          console.log("BAG_PICKED_CUSTOMER");
          return MessageUtil.success({ message: "Order updated" });



        }
      }

      if (status_lalamove == 'completed') {
        if (dadosOrder.metadata.tryOrderType == "STORE_TO_CUSTOMER") {
          tryResponse = this.updateOrderOnTryPlatform(order_db, "BAG_DELIVERED_CUSTOMER");
          return MessageUtil.success({ message: "Order updated" });
        }
        if (dadosOrder.metadata.tryOrderType == "CUSTOMER_TO_STORE") {
          tryResponse = this.updateOrderOnTryPlatform(order_db, "BAG_DELIVERED_STORE");
          return MessageUtil.success({ message: "Order updated" });
        }
      }

      return MessageUtil.success({ message: "Order updated" });


    }

    if (events.eventType == "DRIVER_ASSIGNED") {
      let driver_data_lalamove = events.data.driver;

      const dataRequest = buildParams(driver_data_lalamove);
      console.log(dataRequest);
      return MessageUtil.success({ message: "DRIVER_ASSIGNED_SUCCESS" });
      //return dataRequest;
    }

    if (events.eventType == "WALLET_BALANCE_CHANGED") {
      return MessageUtil.success({
        message: "ERROR -> Evento não está sendo tratado no momento.",
      });
    }

    return MessageUtil.success({ message: "ERROR -> Evento não existe!" });

    //return { event, context };
  }

  updateOrderOnTryPlatform = async (order: any, nextTryOrderStatus: string) => {
    const headerOptions = {
      headers: { Authorization: `Bearer ${getTryApiToken()}` },
    };
    console.log("ORDER REQUEST ID -> ", order.orderRequestId);
    const bodyOptions = {
      //tryOrderNumber: order.tryOrderNumber,
      //  orderRequestId: "64ef4f7b6564a700139f4f08", //order.orderRequestId,
      orderRequestId: order.orderRequestId,//"64f01e5a6564a70013a008f7",//order._id,
      orderNumber: order.tryOrderNumber.toString(),
      status: nextTryOrderStatus,
      driver: {
        name: order.driverName,
        phoneNumber: {
          phoneNo: order.driverPhone,
          countryCode: "55",
          ISO: "BR",
        },
        profilePic: {
          original: order.driverPhotoUrl,
          thumbnail: order.driverPhotoUrl,
          fileName: "deliveryman-003-512.png",
          type: "image/png",
          thumbnailMed: order.driverPhotoUrl,
          processed: "string",
          _id: "string",
        },
        lat: order.driverLat,
        long: order.driverLng,
      },
    };


    /* console.log(
       `--> [PROCESS UPDATE DATA] call try platform: headerOptions`,
       JSON.stringify(headerOptions)
     );
     console.log(
       `--> [PROCESS UPDATE DATA] call try platform: bodyOptions`,
       JSON.stringify(bodyOptions)
     );*/

    // TODO: Revisar atualização via mongose, e verificar se e esse status.
    await orders.findOneAndUpdate(
      { _id: order._id },
      {
        $set: {
          status: bodyOptions.status,
          deliverOrderStatus: bodyOptions.status,
          deliverDeliveryStatus: bodyOptions.status,
          driver: bodyOptions.driver,
        },
      },
      { new: true }
    );

    // TODO: Revisar atualização via endpoint.
    console.log("Body->" + JSON.stringify(bodyOptions));
    console.log("headers->" + JSON.stringify(headerOptions));
    try {
      await axios
        .put(
          `https://backend-api.tryapp.com.br/driver/v3/order`,
          //`http://localhost:8000/driver/v3/order`,
          //https://dev-backend-api.tryapp.com.br
          bodyOptions,
          headerOptions
        )
        .then((response) => {
          return response;
        })
        .catch((err) => {
          console.log(err);
          return err;
        });
    } catch (err) {
      console.log("Erro no put do axios -> ", err);
      return null;
    }

  };
}