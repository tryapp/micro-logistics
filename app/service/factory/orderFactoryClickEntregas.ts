import { orders } from "../../model/orders";
import OrderFactory from "./orderFactory";
import createClickEntregasOrder from "../external/clickEntregas/methods/createOrder";
import { apiCreateResponse } from '../external/clickEntregas/config/types';

export default class OrderFactoryClickEntregas extends OrderFactory {

  getCompanyName(): string {
    return 'ClickEntregas';
  }
  
  async factoryMethod(id: string): Promise<object> {
    let order = await this.getOrder(id);
    
    try {
      const result: any  = await createClickEntregasOrder(order) 
      const resultData  = result.data as apiCreateResponse

      if(resultData.is_successful){
        const clickEntregasOrder = resultData.order

        return orders.findOneAndUpdate(
          { _id: id },
          { $set: {
              companyName: this.getCompanyName(),
              deliverOrderId: clickEntregasOrder.order_id.toString(),
              deliverOrderNumber: clickEntregasOrder.order_id,
              deliverTotalAmount: clickEntregasOrder.payment_amount
            },
            $push: {apiData: clickEntregasOrder} 
          },
          { new: true },
        );
      }else{
        throw new Error("can't create order on clickentregas");
      }
    } catch (error) {
      console.log('[ERROR] -> ', error, JSON.stringify(error));
      throw new Error(error.message);
    }
  }
}