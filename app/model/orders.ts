import mongoose from 'mongoose';

export enum orderStatus {
  new = "new",
  available = "available",
  created = "created",
  active = "active",
  courier_assigned = "courier_assigned",
  parcel_picked_up = "parcel_picked_up",
  courier_departed = "courier_departed",
  courier_arrived = "courier_arrived",
  finished = "finished",
  failed = "failed",
  completed = "completed"
}

export type OrdersDocument = mongoose.Document & {
  // from Try platform
  addressFrom: string,
  addressFromComplement: string,
  addressFromLat: string,
  addressFromLong: string,
  addressTo: string,
  addressToComplement: string,
  addressToLat: string,
  addressToLong: string,
  number: string,
  tryOrderTotalAmount:  number,
  tryOrderPackingListTotalAmount:  number,
  tryStoreId: string,
  tryOrderNumber: string,
  tryOrderType: string,
  tryCustomerFirstName: string,
  tryStoreName: string,
  tryCustomerPhone: string,
  tryStorePhone: string,
  tryArrivalNotified: boolean,
  orderRequestId: string,

  // from 3º Service
  deliverOrderStatus: string,
  deliverDeliveryStatus: string,
  deliverOrderId: string,
  deliverOrderNumber: number,
  deliverTotalAmount: number,
  driverName: string,
  driverLatitude: number,
  driverLongitude: number,
  driverPhotoUrl: string,
  driverPhone: string,
  addressFromRecipientName: string,
  addressFromRecipientPosition: string,
  addressFromPlacePhoto: string,
  addressFromSignPhoto: string,
  addressToRecipientName: string,
  addressToRecipientPosition: string,
  addressToPlacePhoto: string,
  addressToSignPhoto: string,
  
  // from this service
  status: string,
  description: string,
  companyName: string,
  createdAt: Date,
  updatedAt: Date,
  finisheddAt: Date,
  apiData: Array<object>
};

const ordersSchema = new mongoose.Schema({
  addressFrom: {type: String, required: true},
  addressFromComplement: {type: String, required: false},
  addressFromLat: String,
  addressFromLong: String,
  addressTo: {type: String, required: true},
  addressToComplement: {type: String, required: false},
  addressToLat: String,
  addressToLong: String,
  number: String,
  orderRequestId: String,
  tryOrderTotalAmount: {type: Number, required: true},
  tryOrderPackingListTotalAmount: {type: Number, required: true},
  tryStoreId: {type: mongoose.Types.ObjectId, required: true},
  tryOrderNumber: {type: String, required: true},
  tryOrderType: {type: String, required: true},
  tryCustomerFirstName: {type: String, required: true},
  tryCustomerPhone: {type: String, required: true},
  tryStoreName: {type: String, required: true},
  tryStorePhone: {type: String, required: true},
  tryArrivalNotified: {type: Boolean, default: false},

  deliverOrderStatus: String,
  deliverDeliveryStatus: String,
  deliverOrderId: String,
  deliverOrderNumber: Number,
  deliverTotalAmount: Number,
  driverName: String,
  driverLatitude: String,
  driverLongitude: String,
  driverPhotoUrl: String,
  driverPhone: String,
  addressFromRecipientName: String,
  addressFromRecipientPosition: String,
  addressFromPlacePhoto: String,
  addressFromSignPhoto: String,
  addressToRecipientName: String,
  addressToRecipientPosition: String,
  addressToPlacePhoto: String,
  addressToSignPhoto: String,

  status: String,
  description: String,
  companyName: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  finisheddAt: Date,
  apiData: {type: Array, default: []}
});

export const orders = (mongoose.models.orders ||
mongoose.model<OrdersDocument>('orders', ordersSchema, process.env.DB_ORDERS_COLLECTION)
);