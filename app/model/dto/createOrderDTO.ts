export class CreateOrderDTO {
  addressFrom: string;
  addressFromComplement: string;
  addressFromLat: string;
  addressFromLong: string;
  addressTo: string;
  addressToComplement: string;
  addressToLat: string;
  addressToLong: string;
  number: string;
  orderRequestId: string;
  tryOrderTotalAmount:  number;
  tryOrderPackingListTotalAmount:  number;
  tryStoreId: number;
  tryOrderNumber: string;
  tryOrderType: string;
  tryCustomerFirstName: string;
  tryCustomerPhone: string;
  tryStoreName: string;
  tryStorePhone: string;
  status: string; 
}