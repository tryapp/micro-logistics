import { SQSHandler, SQSMessageAttributes } from 'aws-lambda';
// import { orders } from '../model';
//import OrderFactoryClickEntregas from '../service/factory/orderFactoryClickEntregas';
//import OrderFactoryLalaMove from '../service/factory/OrderFactoryLalaMove';

const receiver: SQSHandler = async (event) => {
  try {
    for (const record of event.Records) {
      const messageAttributes: SQSMessageAttributes = record.messageAttributes;
      console.log('Message Attributtes -->  ', messageAttributes.AttributeNameHere.stringValue);
      console.log('Message Body -->  ', record.body);

      let factory;
      const _id = messageAttributes.AttributeNameHere.stringValue;
      const params = JSON.parse(record.body);

      if(params.price > 300){
        //factory = new OrderFactoryClickEntregas();
  //      factory = new OrderFactoryLalaMove();
      }else{
    //    factory = new OrderFactoryLalaMove();
     }

      const result = await factory.factoryMethod(_id);

      console.log('result', result)
    }
  } catch (error) {
    console.log(error);
  }
};

export default receiver;
