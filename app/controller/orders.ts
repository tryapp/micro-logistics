import { Context } from 'aws-lambda';
import { Model } from 'mongoose';
import { MessageUtil } from '../utils/message';
import { OrdersService } from '../service/orders';
import { CreateOrderDTO } from '../model/dto/createOrderDTO';
import { verifyTryApiToken } from '../utils/tryToken';

export class OrdersController extends OrdersService {
  constructor(orders: Model<any>) {
    super(orders);
  }

  /**
   * Create book
   * @param {*} event
   */
  async create (event: any, context?: Context) {
    console.log('functionName', context.functionName);
    /*
     if(!this.isTokenPresent(event)){
      return MessageUtil.error(401, 'No token provided.');
    }

    if(!await verifyTryApiToken(event.headers['x-access-token'])){
      return MessageUtil.error(500, 'Failed to authenticate token.');
    }
    */
  
   
    
    const params: CreateOrderDTO = JSON.parse(event.body);
    
    try {
      const result = await this.createOrder(params);

      // await this.enqueueOrder(result)

      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(422, err.message);
    }
  }

  /**
   * Update a order by id
   * @param event
   */
  async update (event: any, context?: Context) {
    // Função de atualiza a order
    console.log('functionName', context.functionName);

  //  if(!this.isTokenPresent(event)){
  //    return MessageUtil.error(401, 'No token provided.');
  //  }

//    if(!await verifyTryApiToken(event.headers['x-access-token'])){
//      return MessageUtil.error(500, 'Failed to authenticate token.');
//    }
    
    const id: string = event.pathParameters.id;
    const body: object = JSON.parse(event.body);

    try {
      const result = await this.updateOrders(id, body);
      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(err.code, err.message);
    }
  }

  /**
   * Find order list
   */
  async find (event: any) {
    if(!this.isTokenPresent(event)){
      return MessageUtil.error(401, 'No token provided.');
    }
    if(!await verifyTryApiToken(event.headers['x-access-token'])){
      return MessageUtil.error(500, 'Failed to authenticate token.');
    }
    try {
      const result = await this.findOrders();

      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(err.code, err.message);
    }
  }

  /**
   * Query order by id
   * @param event
   */
  async findOne (event: any, context: Context) {
    // The amount of memory allocated for the function
    console.log('memoryLimitInMB: ', context.memoryLimitInMB);

    if(!this.isTokenPresent(event)){
      return MessageUtil.error(401, 'No token provided.');
    }

    if(!await verifyTryApiToken(event.headers['x-access-token'])){
      return MessageUtil.error(500, 'Failed to authenticate token.');
    }

    const id: string = event.pathParameters.id;

    try {
      const result = await this.findOneOrderById(id);

      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(err.code, err.message);
    }
  }

  /**
   * Query order by id
   * @param event
   */
  async findDriverOrder (event: any, context: Context) {
    // The amount of memory allocated for the function
    console.log('memoryLimitInMB: ', context.memoryLimitInMB);
/*
    if(!this.isTokenPresent(event)){
      return MessageUtil.error(401, 'No token provided.');
    }

    if(!await verifyTryApiToken(event.headers['x-access-token'])){
      return MessageUtil.error(500, 'Failed to authenticate token.');
    }
*/
    const id: string = event.pathParameters.id;

    try {
      const result = await this.findDriverIndfoByOrderId(id);

      return MessageUtil.success(result.data);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(500, err.message);
    }
  }

  private isTokenPresent = (event): boolean => {
    return event.headers['x-access-token'];
  }
}
