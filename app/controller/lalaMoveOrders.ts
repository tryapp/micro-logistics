import { Context } from 'aws-lambda';
import { MessageUtil } from '../utils/message';
import UpdateOrder from '../service/external/lalaMove/methods/updateOrder';
import crypto from 'crypto';

export class LalaMoveOrdersControllerSSS {

  async update (event: any, context?: Context) {
    console.log('functionName', context.functionName);
    console.log('eventName', event);
    await new UpdateOrder(JSON.parse(event.body)).process();
    return MessageUtil.success({message: 'Order updated'});
    /*
          try {
      console.log('---> [ClickEntregas] json body', event.body);
      
      await new UpdateOrder(JSON.parse(event.body)).process();
      
      return MessageUtil.success({message: 'Order updated'});
    } catch (err) {
      console.error(err);

     // return MessageUtil.error(err.code, err.message);
    }
   // return MessageUtil.success(200,{message: 'Order updated'});
    */
  }
}
