import { Context } from 'aws-lambda';
import { MessageUtil } from '../utils/message';
import UpdateOrder from '../service/external/clickEntregas/methods/updateOrder';
import crypto from 'crypto';
export class ClickEntregasOrdersController {

  async update (event: any, context?: Context) {
    console.log('EVENT ---->', event);
    console.log('CONTEXT -->', context);
    console.log('functionName', context.functionName);
    
    if(!this.isSignaturePresent(event)){
      console.log('---> [ERROR] signature not present', event.headers['x-dv-signature'], event.headers);

      return MessageUtil.error(403, 'Signature is not present.');
    }

    if(!this.isSignatureValid(event)){
      console.log('---> [ERROR] invalid signature', event.headers['x-dv-signature']);

      return MessageUtil.error(403, 'Signature is not valid.');
    }

    try {
      console.log('---> [ClickEntregas] json body', event.body);
      
      await new UpdateOrder(JSON.parse(event.body)).process();
      
      return MessageUtil.success({message: 'Order updated'});
    } catch (err) {
      console.error(err);

      return MessageUtil.error(err.code, err.message);
    }
  }

  private isSignaturePresent = (event: any): boolean => {
    return event.headers['x-dv-signature']
  }

  private isSignatureValid = (event: any): boolean => {
    const signature = crypto.createHmac('sha256', process.env.CLICKENTREGAS_CALLBACK_TOKEN).update(event.body).digest().toString('hex')

    console.log("---> Received signature, my signature", event.headers['x-dv-signature'], signature)

    return event.headers['x-dv-signature'] === signature
  }
}
