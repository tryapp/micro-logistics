export const find = [
    {
        "_id": "60b58a709039577f497c4ca6",
        "addressFrom": "Rua dos Alpes 120, ap 95",
        "addressTo": "Rua Visconde de cairu 134",
        "tryOrderTotalAmount": 200,
        "tryOrderPackingListTotalAmount": 200,
        "tryStoreId": "5fd9f9b5f54edd4244483b64",
        "tryOrderNumber": 1231,
        "tryOrderType": "STORE_TO_CUSTOMER",
        "status": "created",
        "createdAt": "2021-06-01T01:16:32.732Z",
        "updatedAt": "2021-06-01T01:16:32.732Z",
        "__v": 0
    },
    {
        "_id": "60b58b6e43b61400095d21eb",
        "addressFrom": "rua agostinho gomes 633",
        "addressTo": "rua dos alpes 120",
        "tryOrderTotalAmount": 20,
        "tryOrderPackingListTotalAmount": 20,
        "tryStoreId": "5fd9f9b5f54edd4244483b64",
        "tryOrderNumber": 1456,
        "tryOrderType": "STORE_TO_CUSTOMER",
        "status": "created",
        "createdAt": "2021-06-01T01:20:46.577Z",
        "updatedAt": "2021-06-01T01:20:46.578Z",
        "__v": 0
    }
]

export const findError = new Error('test find error');

export const create = {
    "_id": "60b56a2ed19bcc00080c2a6d",
    "addressFrom": "Rua dos Alpes 120, ap 95",
    "addressTo": "Rua Visconde de cairu 134",
    "tryOrderTotalAmount": 200,
    "tryOrderPackingListTotalAmount": 200,
    "tryStoreId": "5fd9f9b5f54edd4244483b64",
    "tryOrderNumber": 1231,
    "tryOrderType": "STORE_TO_CUSTOMER",
    "createdAt": "2021-05-31T22:58:54.688Z",
    "updatedAt": "2021-05-31T22:58:54.688Z",
    "__v": 0
}

export const createError = new Error('test error on create Order');

export const findOne = {
  "_id": "60b56a2ed19bcc00080c2a6d",
    "addressFrom": "Rua dos Alpes 120, ap 95",
    "addressTo": "Rua Visconde de cairu 134",
    "tryOrderTotalAmount": 200,
    "tryOrderPackingListTotalAmount": 200,
    "tryStoreId": "5fd9f9b5f54edd4244483b64",
    "tryOrderNumber": 1231,
    "tryOrderType": "STORE_TO_CUSTOMER",
    "createdAt": "2021-05-31T22:58:54.688Z",
    "updatedAt": "2021-05-31T22:58:54.688Z",
    "__v": 0
};

export const castError = new Error('Cast to number failed for value "NaN" at path "id" for model "Orders"');