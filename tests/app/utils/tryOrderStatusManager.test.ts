import { expect } from 'chai';
import { getOrderNextStatus } from "../../../app/utils/tryOrderStatusManager";

describe('getOrderNextStatus', () => {
  describe('when status = courier_assigned', () => {
    it('is ACCEPTED when currentStatus is planned', () => {
      expect(getOrderNextStatus('courier_assigned', 'planned')).to.equal("ACCEPTED");
    })

    it('is undefined when currentStatus != planned', () => {
      expect(getOrderNextStatus('courier_assigned', 'active')).to.equal(undefined);
    })
  })

  describe('when status = courier_departed', () => {
    it('is REACHED_STORE when currentStatus is courier_assigned and way is STORE_TO_CUSTOMER', () => {
      expect(getOrderNextStatus('courier_departed', 'courier_assigned', 'STORE_TO_CUSTOMER')).to.equal("REACHED_STORE");
    })

    it('is REACHED_CUSTOMER when currentStatus is courier_assigned and way is CUSTOMER_TO_STORE', () => {
      expect(getOrderNextStatus('courier_departed', 'courier_assigned', 'CUSTOMER_TO_STORE')).to.equal("REACHED_CUSTOMER");
    })

    it('is undefined when currentStatus != courier_assigned', () => {
      expect(getOrderNextStatus('courier_departed', 'active')).to.equal(undefined);
    })
  })

  describe('when status = parcel_picked_up', () => {
    it('is GOT_BAG_STORE when currentStatus is courier_assigned and way is STORE_TO_CUSTOMER', () => {
      expect(getOrderNextStatus('parcel_picked_up', 'courier_departed', 'STORE_TO_CUSTOMER')).to.equal("GOT_BAG_STORE");
    })

    it('is GOT_BAG_CUSTOMER when currentStatus is courier_assigned and way is CUSTOMER_TO_STORE', () => {
      expect(getOrderNextStatus('parcel_picked_up', 'courier_departed', 'CUSTOMER_TO_STORE')).to.equal("GOT_BAG_CUSTOMER");
    })

    it('is undefined when currentStatus != courier_assigned', () => {
      expect(getOrderNextStatus('parcel_picked_up', 'active')).to.equal(undefined);
    })
  })

  describe('when status = active', () => {
    it('is BAG_PICKED_STORE when currentStatus is parcel_picked_up and way is STORE_TO_CUSTOMER', () => {
      expect(getOrderNextStatus('active', 'parcel_picked_up', 'STORE_TO_CUSTOMER')).to.equal("BAG_PICKED_STORE");
    })

    it('is BAG_PICKED_CUSTOMER when currentStatus is parcel_picked_up and way is CUSTOMER_TO_STORE', () => {
      expect(getOrderNextStatus('active', 'parcel_picked_up', 'CUSTOMER_TO_STORE')).to.equal("BAG_PICKED_CUSTOMER");
    })

    it('is undefined when currentStatus != parcel_picked_up', () => {
      expect(getOrderNextStatus('active', 'active')).to.equal(undefined);
    })
  })

  describe('when status = courier_arrived', () => {
    it('is undefined when currentStatus is active', () => {
      expect(getOrderNextStatus('courier_arrived', 'active')).to.equal(undefined);
    })

    it('is undefined when currentStatus != parcel_picked_up', () => {
      expect(getOrderNextStatus('courier_arrived', 'active')).to.equal(undefined);
    })
  })

  describe('when status = finished', () => {
    it('is BAG_DELIVERED_ when currentStatus is parcel_picked_up and way is STORE_TO_CUSTOMER', () => {
      expect(getOrderNextStatus('finished', 'courier_arrived', 'STORE_TO_CUSTOMER')).to.equal("BAG_DELIVERED_CUSTOMER");
    })

    it('is BAG_DELIVERED_ when currentStatus is parcel_picked_up and way is CUSTOMER_TO_STORE', () => {
      expect(getOrderNextStatus('finished', 'courier_arrived', 'CUSTOMER_TO_STORE')).to.equal("BAG_DELIVERED_STORE");
    })

    it('is undefined when currentStatus != parcel_picked_up', () => {
      expect(getOrderNextStatus('finished', 'active')).to.equal(undefined);
    })
  })
})