// import lambdaTester from 'lambda-tester';
// import { expect } from 'chai';
// import { findOrders, createOrder, findOneOrder } from '../app/handler';
// import * as orderMocks from './orders.mock';
// import { orders as OrdersModel } from '../app/model/orders';
// import sinon from 'sinon';

// describe('Find Orders [GET]', () => {
//   let s;

//   beforeEach(() => {
//     s = sinon.mock(OrdersModel);
//   })

//   afterEach(() => {
//     s.restore();
//   })
  
//   it('success', () => {
//     s.expects('find')
//       .resolves(orderMocks.find);
//       return lambdaTester(findOrders)
//       .event({})
//       .expectResult((result: any) => {
//         expect(result.statusCode).to.equal(200);
//         const body = JSON.parse(result.body);
//         expect(body.code).to.equal(200);
//         expect(body.data.length).to.equal(2);
//       })
//   })

//   it('error', () => {
//     s.expects('find').rejects(orderMocks.findError);

//     return lambdaTester(findOrders)
//     .event({})
//     .expectResult((result: any) => {
//       expect(result.statusCode).to.equal(200);
//       const body = JSON.parse(result.body);
//       expect(body.code).to.equal(1000);
//     });
//   })
// })

// describe('Create [POST]', () => {
//   let s;

//   beforeEach(() => {
//     s = sinon.mock(OrdersModel);
//   })

//   afterEach(() => {
//     s.restore();
//   })

//   it('success', () => {
//     s.expects('create').resolves(orderMocks.create);

//     return lambdaTester(createOrder)
//       .event({ body: JSON.stringify({
//         addressFrom: "Rua dos Alpes 120, ap 95",
//         addressTo: "Rua Visconde de cairu 134",
//         tryOrderTotalAmount: "200",
//         tryOrderPackingListTotalAmount: "200",
//         tryStoreId: "5fd9f9b5f54edd4244483b64",
//         tryOrderNumber: "1231",
//         tryOrderType: "STORE_TO_CUSTOMER"
//       })})
//       .expectResult((result: any) => {
//         expect(result.statusCode).to.equal(200);
//         const body = JSON.parse(result.body);
//         expect(body.code).to.equal(200);
//       });
//   });

//   it('error', () => {
//     s.expects('create').rejects(orderMocks.createError);

//     return lambdaTester(createOrder)
//       .event({ body: JSON.stringify({
//         addressFrom: "Rua dos Alpes 120, ap 95",
//         addressTo: "Rua Visconde de cairu 134"
//       })})
//       .expectResult((result: any) => {
//         expect(result.statusCode).to.equal(200);
//         const body = JSON.parse(result.body);
//         expect(body.code).to.equal(422);
//       });
//   });
// });

// describe('FindOne [GET]', () => {
//   let s;

//   beforeEach(() => {
//     s = sinon.mock(OrdersModel);
//   })

//   afterEach(() => {
//     s.restore();
//   })

//   it('success', () => {
//     try {
//       s.expects('findOne')
//         .atLeast(1)
//         .atMost(3)
//         .resolves(orderMocks.findOne);

//       return lambdaTester(findOneOrder)
//       .event({ pathParameters: { id: 25768396 } })
//       .expectResult((result: any) => {
//         expect(result.statusCode).to.equal(200);
//         const body = JSON.parse(result.body);
//         expect(body.code).to.equal(200);
//         s.verify();
//       });
//     } catch (err) {
//       console.log(err);
//     }
//   });

//   it('error', () => {
//     try {
//       s.expects('findOne')
//         .rejects(orderMocks.castError);

//       return lambdaTester(findOneOrder)
//       .event({ pathParameters: { id: 25768396 } })
//       .expectResult((result: any) => {
//         expect(result.statusCode).to.equal(200);
//         const body = JSON.parse(result.body);
//         expect(body.code).to.equal(1000);
//       });
//     } catch (err) {
//       console.log(err);
//     }
//   });
// });